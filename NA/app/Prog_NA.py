import crypto_anto
import time

#definition des variabes necessaires 
myself = "NA"
mycert = f"{myself}.crt"
myprivatekey = f"{myself}_private_key.key"
mypublickey = f"{myself}_public_key.pub"

print (crypto_anto.poussermyself(myself))   #obligatoire voir fichier word
print (crypto_anto.gen_keys(myprivatekey, mypublickey))


input("voulez vous demnader un certificat?")
socket = crypto_anto.open_socket_connection("sca", 5000)
print (crypto_anto.send_cert_request(socket))
crypto_anto.close_socket_connection(socket)


input("voulez vous envoyer un hello request à NB ?")
socket = crypto_anto.open_socket_connection("nb", 5000)
print (crypto_anto.send_hello_request(socket))


print("test de la revocation du certificat")
socketSCA = crypto_anto.open_socket_connection("sca", 5000)     #ouverture d'un autre socket pour faire la vérification aupres de SCA
if crypto_anto.send_is_revoked_request(socketSCA, "NB.crt"):
    print ("revoqué")
else:
    print ("non revoqué")

print (crypto_anto.close_socket_connection(socketSCA))


input("veux tu générer la clé symétrique ?")
print (crypto_anto.gen_symmetric_key("NB_symetric_key.key"))

input("voulez vous envoyer la clé symétrique ?")
print (crypto_anto.send_symetric_key(socket, "NB_symetric_key.key", "NB_public_key.pub"))

input("veux tu envoyer le message ?")
print (crypto_anto.send_message(socket, crypto_anto.sym_encrypt_message("ET home phone", "NB_symetric_key.key")))

print (crypto_anto.close_socket_connection(socket))