import crypto_anto
import socket

#definition des variabes necessaires 
myself = 'SCA'
myprivatekey = f"{myself}_private_key.key"
root_cert_filename = 'SCA_ROOT.crt'

print (crypto_anto.poussermyself(myself)) # obligatoire voir fichier word

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)   #creation du socket
server_socket.bind(('', 5000))
server_socket.listen()

while True:    #boucle infinie pour accepter les connexions et ecouter les messages
    client_socket, address = server_socket.accept()
    
    try:
        print(crypto_anto.receive_message(client_socket))
    except socket.error as e:
        print(f"Erreur durant la reception du message : {e} ")
