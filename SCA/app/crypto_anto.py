from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography import x509
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.fernet import Fernet
from base64 import b64encode, b64decode
import base64
import datetime
import time
import socket
import os

# Définir les codes de couleur
RED = "\033[31m"
GREEN = "\033[32m"
RESET = "\033[0m"

def poussermyself(nom): # Cette fonction doit obligatoirement figurer au debut des fichiers Prog_noeud.py.
    try:
        global myself
        global mycert
        global myprivatekey
        global mypublickey
        global root_cert_fn

        myself = nom
        mycert = f"{myself}.crt"
        myprivatekey = f"{myself}_private_key.key"
        mypublickey = f"{myself}_public_key.pub"
        root_cert_fn = 'SCA_ROOT.crt'
        return (f"{GREEN}Fonction poussermyself : Les variables sont initialisées{RESET}")
    except Exception as e:
        return (f"{RED}Fonction poussermyself : {e}{RESET}")

def gen_keys(private_key_fn, public_key_fn):
    try:
        private_key = rsa.generate_private_key( # Générer une paire de clés RSA
            public_exponent=65537,
            key_size=2048,
            backend=default_backend()
        )
        private_key_pem = private_key.private_bytes( # Sérialiser la clé privée au format PEM
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption()
        )    
        public_key = private_key.public_key() # Sérialiser la clé publique au format PEM
        public_key_pem = public_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )

        with open(private_key_fn, "wb") as private_key_file: # Écrire la clé privée dans un fichier
            private_key_file.write(private_key_pem)

        with open(public_key_fn, "wb") as public_key_file: # Écrire la clé publique dans un fichier
            public_key_file.write(public_key_pem)

        return (f"{GREEN} Fonction gen_keys : Clés générées avec succès{RESET}")
    except Exception as e:
        return (f"{RED} Fonction gen_keys : {e} {RESET}")
        

def gen_root_cert(private_key_fn, root_cert_fn):
    try:

        key = rsa.generate_private_key( # Créer une nouvelle clé privée RSA
            public_exponent=65537,
            key_size=2048,
            backend=default_backend()
        )   
        subject = issuer = x509.Name([ # Créer un certificat auto-signé
            x509.NameAttribute(NameOID.COUNTRY_NAME, u"FR"),
            x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, u"Vosges"),
            x509.NameAttribute(NameOID.LOCALITY_NAME, u"Plombieres-les-Bains"),
            x509.NameAttribute(NameOID.ORGANIZATION_NAME, u"Anto-Corp"),
            x509.NameAttribute(NameOID.ORGANIZATIONAL_UNIT_NAME, u"IT Department"),
            x509.NameAttribute(NameOID.COMMON_NAME, u"testcert"),
        ])
        cert = x509.CertificateBuilder().subject_name(
            subject
        ).issuer_name(
            issuer
        ).public_key(
            key.public_key()
        ).serial_number(
            x509.random_serial_number()
        ).not_valid_before(
            datetime.datetime.utcnow()
        ).not_valid_after(
            datetime.datetime.utcnow() + datetime.timedelta(days=365)
        ).sign(key, hashes.SHA256(), default_backend())

        with open(private_key_fn, "wb") as keyfile: # Écrire la clé privée et le certificat dans des fichiers
            keyfile.write(key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.TraditionalOpenSSL,
                encryption_algorithm=serialization.NoEncryption(),
            ))
        with open(root_cert_fn, "wb") as certfile:
            certfile.write(cert.public_bytes(serialization.Encoding.PEM))

        return (f"{GREEN}Fonction gen_root_cert : Certificat racine généré avec succès{RESET}")
    except Exception as e:
        return (f"{RED} Fonction gen_root_cert : {e} {RESET}")

def sign_certificate_with_root(root_cert_fn, private_key_fn, key2sign_fn, dest_cert_fn):
    try:
        with open(root_cert_fn, "rb") as f:         # Charger le certificat SCA
            ca_cert = x509.load_pem_x509_certificate(f.read(), default_backend())

        with open(private_key_fn, "rb") as f:       # Charger la clé privée de SCA
            ca_key = serialization.load_pem_private_key(f.read(), password=None, backend=default_backend())

        with open(key2sign_fn, "rb") as f:          # Charger la clé publique du demandeur
            subject_key = serialization.load_pem_public_key(f.read(), backend=default_backend())

        subject_cert = x509.CertificateBuilder().subject_name(      # Créer un certificat pour le sujet
            ca_cert.subject
        ).issuer_name(
            ca_cert.subject
        ).public_key(
            subject_key
        ).serial_number(
            x509.random_serial_number()
        ).not_valid_before(
            datetime.datetime.utcnow()
        ).not_valid_after(
            datetime.datetime.utcnow() + datetime.timedelta(days=365)
        ).sign(ca_key, hashes.SHA256(), default_backend())

        with open(dest_cert_fn, "wb") as f:                         # Écrire le certificat du sujet dans un fichier PEM
            f.write(subject_cert.public_bytes(serialization.Encoding.PEM))
        return (f"{GREEN}Fonction sign_certificate_with_root : Certificat signé avec succès{RESET}")
    except Exception as e:
        return (f"{RED} Fonction sign_certificate_with_root : {e} {RESET}")

def verify_certificate(root_cert_fn, cert2verify_fn):
    try:
        with open(root_cert_fn, "rb") as root_crt_file:         # Charger le certificat racine
            root_crt = x509.load_pem_x509_certificate(root_crt_file.read(), default_backend())

        with open(cert2verify_fn, "rb") as crt_file:            # Charger le certificat à vérifier
            crt = x509.load_pem_x509_certificate(crt_file.read(), default_backend())
    
        pub_key = root_crt.public_key()     # Vérifier la signature du certificat avec la clé publique du certificat racine
        pub_key.verify(
            crt.signature,
            crt.tbs_certificate_bytes,
            padding.PKCS1v15(),
            crt.signature_hash_algorithm,
        )
        print(f"{GREEN}Fonction verify_certificate : Certificat valide{RESET}")
        return True
    except:
        print(f"{RED}Fonction verify_certificate : Certificat invalide{RESET}")
        return False

def as_encrypt_message(message, public_key_filename):
    try:
        with open(public_key_filename, "rb") as key_file:           # Charger la clé publique depuis le fichier
            public_key_data = key_file.read()
            public_key_obj = serialization.load_pem_public_key(public_key_data, backend=default_backend())

        encrypted_message = public_key_obj.encrypt(                 # Chiffrer le message avec la clé publique
            message.encode(),
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )
        print(f"{GREEN}Fonction as_encrypt_message : message chiffré avec succès{RESET}")
        return base64.urlsafe_b64encode(encrypted_message).decode()     # Convertir le message chiffré en str
    except Exception as e:
        print(f"{RED}Fonction as_encrypt_message : {e}{RESET}")


def as_decrypt_message(encrypted_message, private_key_filename):
    try:
        with open(private_key_filename, "rb") as key_file:      # Charger la clé privée depuis le fichier
            private_key_data = key_file.read()
            private_key_obj = serialization.load_pem_private_key(private_key_data, password=None, backend=default_backend())

        encrypted_message_bytes = base64.urlsafe_b64decode(encrypted_message)       # Convertir le message chiffré en bytes

        decrypted_message = private_key_obj.decrypt(        #dechiffrer le message avec la clé privée
            encrypted_message_bytes,
            padding.OAEP(
                mgf=padding.MGF1(hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )
        print(f"{GREEN}Fonction as_decrypt_message : message déchiffré avec succès{RESET}")
        return decrypted_message.decode()
    except Exception as e:
        print(f"{RED}Fonction as_decrypt_message : {e}{RESET}")


def sign_message(message, private_key_fn):
    try:
        with open(private_key_fn, "rb") as keyfile:             # Charger la clé privée depuis le fichier
            private_key = serialization.load_pem_private_key(
                keyfile.read(),
                password=None,
                backend=default_backend()
            )
        message = message.encode('utf-8')               # Signer le message avec la clé privée
        signature = private_key.sign(
            message,
            padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH
            ),
            hashes.SHA256()
        )
        print(f"{GREEN}Fonction sign_message : message signé avec succès{RESET}")
        return base64.b64encode(signature)              # Encode the signature in base64 before returning
    except Exception as e:
        print(f"{RED}Fonction sign_message : {e}{RESET}")

def verify_signature(message, signature, public_key_fn, date_str):
    
    date = datetime.datetime.strptime(date_str, "%Y-%m-%d %H:%M") # Convertir la chaîne de caractères en un objet datetime
    maintenant = datetime.datetime.now() # Obtenir l'heure actuelle
    diff = maintenant - date # Calculer la différence entre l'heure actuelle et la date_heure
    
    if abs(diff) <= datetime.timedelta(minutes=5): # Vérifier si la différence est inférieure à 5 minutes

        with open(public_key_fn, "rb") as keyfile: # Charger la clé publique depuis le fichier  
            public_key = serialization.load_pem_public_key(
                keyfile.read(),
                backend=default_backend()
            )
        signature = base64.b64decode(signature)     # Decode the signature from base64
        message = message.encode('utf-8')
        try:
            public_key.verify(
                signature,                      # Vérifier la signature du message avec la clé publique
                message,
                padding.PSS(
                    mgf=padding.MGF1(hashes.SHA256()),
                    salt_length=padding.PSS.MAX_LENGTH
                ),
                hashes.SHA256()
            )
            print(f"{GREEN}Fonction verify_signature : signature valide{RESET}")
            return True
        except:
            print(f"{RED}Fonction verify_signature : signature invalide{RESET}")
            return False
    else:
        print(f"{RED}La date date de plus de 5 minutes{RESET}")
        return False

def verify_signature_with_certificate(certificate_fn, message, signature, date_str):

    date = datetime.datetime.strptime(date_str, "%Y-%m-%d %H:%M")       # Convertir la chaîne de caractères en un objet datetime
    maintenant = datetime.datetime.now()        # Obtenir l'heure actuelle
    diff = maintenant - date        # Calculer la différence entre l'heure actuelle et la date_heure
    
    if abs(diff) <= datetime.timedelta(minutes=5):      # Vérifier si la différence est inférieure à 5 minutes

        
        with open(certificate_fn, "rb") as cert_file:       # Charger le certificat depuis le fichier
            cert_data = cert_file.read()
            certificate = x509.load_pem_x509_certificate(cert_data, default_backend())
            public_key = certificate.public_key()

        signature = base64.b64decode(signature)         # decoder la signature en base64
        message = message.encode('utf-8')

        try:
            public_key.verify(              # Vérifier la signature du message avec la clé publique du certificat
                signature,
                message,
                padding.PSS(
                    mgf=padding.MGF1(hashes.SHA256()),
                    salt_length=padding.PSS.MAX_LENGTH
                ),
                hashes.SHA256()
            )
            print(f"{GREEN}Fonction verify_signature_with_certificate : signature valide{RESET}")
            return True
        except Exception:
            print(f"{RED}Fonction verify_signature_with_certificate : signature invalide{RESET}")
            return False
    else:
        print(f"{RED}La date date de plus de 5 minutes{RESET}")
        return False

def is_string_in_revoked(string):
    try:
        for filename in os.listdir("Revoked"):          # Parcourir les fichiers dans le dossier Revoked
            if filename.endswith(".crt"):
                with open(os.path.join("Revoked", filename), 'r') as file: # Ouvrir le fichier en mode lecture
                    if string in file.read():       # Vérifier si le certificat est dans la liste des certificats révoqués
                        print(f"{RED}Fonction is_string_in_revoked : Certificat trouvé dans la liste des certificats révoqués{RESET}")
                        return True
        print(f"{GREEN}Fonction is_string_in_revoked : Certificat non trouvé dans la liste des certificats révoqués{RESET}")
        return False
    except Exception as e:
        print(f"{RED}Fonction is_string_in_revoked : {e}{RESET}")
        

def gen_symmetric_key(symetric_key_fn):
    try:
        key = Fernet.generate_key()
        with open(symetric_key_fn, 'w') as key_file:  # ouvrir le fichier en mode écriture
            key_file.write(key.decode())  # Convertir la clé en str avant de l'écrire
        return (f"{GREEN}Fonction generate_symmetric_key : Clé symétrique générée avec succès{RESET}")
    except Exception as e:
        return (f"{RED}Fonction generate_symmetric_key : {e}{RESET}")   

def sym_encrypt_message(message, key_file_path):
    try:
        with open(key_file_path, 'r') as key_file:
            str_key = key_file.read()  # lire la clé comme str
        key = str_key.encode()  # Convertir str en bytes
        cipher_suite = Fernet(key)
        cipher_text = cipher_suite.encrypt(message.encode())
        print(f"{GREEN}Fonction sym_encrypt_message : message chiffré avec succès{RESET}")
        return cipher_text.decode()  # Convert bytes to str
    except Exception as e:
        retrun(f"{RED}Fonction sym_encrypt_message : {e}{RESET}")

def sym_decrypt_message(cipher_text, key_file_path):
    try:
        with open(key_file_path, 'r') as key_file:
            str_key = key_file.read()  # lire la clé comme str
        key = str_key.encode()  # Convertir str en bytes
        cipher_suite = Fernet(key)
        plain_text = cipher_suite.decrypt(cipher_text.encode())
        print(f"{GREEN}Fonction sym_decrypt_message : message déchiffré avec succès{RESET}")
        return plain_text.decode()  # Convertir bytes to str
    except Exception as e:
        return(f"{RED}Fonction sym_decrypt_message : {e}{RESET}")



#############
#fin partie crypto, debut partie messages


def open_socket_connection(host, port):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    # Creation d'un objet socket
        sock.connect((host, port))  # Connexion au serveur

        return sock         # Return le socket
    except Exception as e:
        print(f"{RED}Fonction open_socket_connection : {e}{RESET}")
        return None

def accept_socket_connection(port):
    while True:
        try:
            server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)     # Creation d'une variable socket
            server_sock.bind(('', port))        # Lier le socket à l'adresse et au port
            server_sock.listen(1)       #   Ecouter les connexions entrantes
        
            print("Waiting for a connection...")
            client_sock, client_address = server_sock.accept() # Accepter la connexion entrante
            print(f"{GREEN}Connection established with {client_address}{RESET}")
            
            server_sock.close() # Fermer les autres socket ouverts par sécurité
            
            return client_sock      # retourner le socket client
        except Exception as e:
            print(f"{RED}Fonction accept_socket_connection : {e}{RESET}")
            if server_sock:
                server_sock.close()
            continue  # Retry accepting connection


def receive_message(socket):
    received_data = ""
    try:
        while True: # Boucle de réception
            data = socket.recv(1024)  # Recevoir des données par tranches de 1024 octets
            try:
                received_data += f"{data.decode()}" #stockage des données reçues
            except Exception as e:
                print(f"{RED}Error occurred while decoding received data: {e}{RESET}")

            if "_End_" in f"{data.decode()}": #si le message contient le mot clé _End_ on sort
                break
        
        messages = received_data.split("_Intersection_")                # Diviser les messages en fonction du délimiteur
        if messages[0] == "_Hello_Request_":                            #detection d'un message hello Request 
            with open(f"{messages[1]}_public_key.pub", "w") as file:    # sauvegarde de la clé publique
                file.write(messages[2])
            with open(f"{messages[1]}.crt", "w") as file:               # sauvegarde du certificat
                file.write(messages[3])
            if verify_certificate(root_cert_fn, f"{messages[1]}.crt"):  # vérification du certificat à partir du certificat racine
                send_hello_reply(socket)                                # envoi d'un message hello reply        
                return f"{GREEN}certificat correct et valide{RESET}"
            else:
                os.remove(f"{messages[1]}.crt")                         # suppression du certificat en cas d'usurpation
                return f"{RED}certificat incorrect : usurpation de l'autorité de certification{RESET}"

        if messages[0] == "_Hello_Reply_":                              #detection d'un message hello Reply 
            with open(f"{messages[1]}_public_key.pub", "w") as file:    # sauvegarde de la clé publique
                file.write(messages[2])
            with open(f"{messages[1]}.crt", "w") as file:               # sauvegarde du certificat
                file.write(messages[3])
            if verify_certificate(root_cert_fn, f"{messages[1]}.crt"):  # vérification du certificat à partir du certificat racine
                return f"{GREEN}certificat correct et valide{RESET}"
            else:
                os.remove(f"{messages[1]}.crt")                         # suppression du certificat en cas d'usurpation
                return f"{RED}certificat incorrect : usurpation de l'autorité de certification{RESET}"
        
        if messages[0] == "_Cert_Request_":                             #detection d'un cert request
            with open(f"{messages[1]}_public_key.pub", "w") as file:    # sauvegarde de la clé publique
                file.write(messages[2])
            try:
                sign_certificate_with_root(root_cert_fn, myprivatekey, f"{messages[1]}_public_key.pub", f"{messages[1]}.crt")       # signature du certificat
            except Exception as e:
                return(f"{RED}Errur durant la signature de la clé : {e}")   
            send_cert_reply(socket, f"{messages[1]}.crt")               # envoi du certificat signé
            return f"{GREEN}certificat signé et envoyé{RESET}"

        if messages[0] == "_Cert_Reply_":                               #detection d'un message cert reply   
            with open(mycert, "w") as file:                             # sauvegarde du certificat
                file.write(messages[2])
            if verify_certificate(root_cert_fn, mycert):                # vérification du certificat à partir du certificat racine
                return (f"{GREEN}certificat recu et valide{RESET}")
            else:
                os.remove(mycert)                                       # suppression du certificat en cas d'usurpation
                return (f"{RED}certificat incorrect : usurpation de l'autorité de certification{RESET}")
        
        if messages[0] == "_Is_Revoked_Request_":                       #detection d'un message is revoked request
            if is_string_in_revoked(messages[2]):                       # vérification si le certificat n'est pas révoqué
                send_is_revoked_reply(socket, "True", messages[2])      
            else:
                send_is_revoked_reply(socket, "False", messages[2])
        
        if messages[0] == "_Is_Revoked_Reply_":                         #detection d'un message is revoked reply           
            if verify_signature_with_certificate(root_cert_fn, messages[1] + messages[2] + messages[3] + messages[4], messages[5], messages[4]):    # vérification de la signature
                print (f"{GREEN}signature correcte{RESET}")
                if messages[2] == "True":
                    print (f"{RED}le certificat est révoqué{RESET}")    
                    return True
                elif messages[2] == "False":
                    print (f"{GREEN}le certificat n'est pas révoqué{RESET}")
                    return False
        
        if messages[0] == "_Send_Symetric_Key_":                        #detection d'un message send symetric key
            with open(f"{messages[1]}_symetric_key.key", "w") as file:
                file.write(as_decrypt_message(messages[2], myprivatekey))   # ecriture de la clé déchifrée
        
        if messages[0] == "_Message_":                                      #detection d'un message message
            if verify_signature(messages[1] + messages[2] + messages[3], messages[4], f"{messages[1]}_public_key.pub", messages[3]):        #verification de la signature
                print ("signature correcte")
                return sym_decrypt_message(messages[2], f"{messages[1]}_symetric_key.key")  #return du message déchiffré

        return f"{GREEN}fonction recive_message terminée avec succès{RESET}"

    except Exception as e:
        time.sleep(1)
        return (f"{RED}fonction recive_message: {e}{RESET}")

def close_socket_connection(socket):
    try:
        socket.close()
        return (f"{GREEN}Fonction close_socket_connection : Connexion fermée avec succès{RESET}")
    except Exception as e:
        return (f"{RED}Fonction close_socket_connection : {e}{RESET}")

def send_hello_request(socket):
    try:
        
        message = "_Hello_Request_"                     # type de message
        intersection  = "_Intersection_"
        end = "_End_"
        with open(mypublickey, "rb") as key_file:       # Charger la clé publique depuis le fichier
            public_key_data = key_file.read()
            public_key_obj = serialization.load_pem_public_key(public_key_data, backend=default_backend())

        with open(mycert, "rb") as cert_file:           # Charger le certificat depuis le fichier
            cert_data = cert_file.read()

        # Envoyer le message, la clé publique et le certificat
        socket.sendall(message.encode())        # 0
        socket.sendall(intersection.encode())  
        socket.sendall(myself.encode())         # 1 source
        socket.sendall(intersection.encode())  
        socket.sendall(public_key_data)         # 2 clé publique
        socket.sendall(intersection.encode())  
        socket.sendall(cert_data)               # 3 mon certificat
        socket.sendall(intersection.encode())  
        socket.sendall(end.encode())
        print (f"{GREEN}hello request envoyé{RESET}")
        print (receive_message(socket))         # affichage de la réponse recue 
        return f"{GREEN}Hello handshake completed successfully{RESET}"
    except Exception as e:
        return(f"{RED}Erreur durant l'envoi de hello_request: {e}{RESET}")

def send_hello_reply(socket): #meme fonction que request mais avec un message différent
    try:
        message = "_Hello_Reply_"                   # type de message
        intersection  = "_Intersection_"
        end = "_End_"
        with open(mypublickey, "rb") as key_file:       # Charger la clé publique depuis le fichier
            public_key_data = key_file.read()
            public_key_obj = serialization.load_pem_public_key(public_key_data, backend=default_backend())

        with open(mycert, "rb") as cert_file:       # Charger le certificat depuis le fichier
            cert_data = cert_file.read()

        # Envoyer le message, la clé publique et le certificat
        socket.sendall(message.encode())    # 0
        socket.sendall(intersection.encode())  
        socket.sendall(myself.encode())         # 1 source
        socket.sendall(intersection.encode())  
        socket.sendall(public_key_data)         # 2 clé publique
        socket.sendall(intersection.encode())  
        socket.sendall(cert_data)               # 3 mon certificat
        socket.sendall(intersection.encode())  
        socket.sendall(end.encode())
        return f"{GREEN} Hello_reply sent successfully{RESET}"
    except Exception as e:
        return(f"{RED}Erreur durant l'envoi de hello_reply: {e}{RESET}")

def send_cert_request(socket):
    try:
        message = "_Cert_Request_"              # type de message
        intersection  = "_Intersection_"
        end = "_End_"
        with open(mypublickey, "rb") as key_file:       # Charger la clé publique depuis le fichier
            public_key_data = key_file.read()

        socket.sendall(message.encode())        # 0
        socket.sendall(intersection.encode())   
        socket.sendall(myself.encode())         # 1 source
        socket.sendall(intersection.encode())  
        socket.sendall(public_key_data)         # 2 ma clé publique
        socket.sendall(intersection.encode()) 
        socket.sendall(end.encode())

        print (f"{GREEN}demande de certificat envoyée avec succès{RESET}")
        print(receive_message(socket))                  # affichage de la réponse recue
        return f"{GREEN}Certificat recu avec succès{RESET}"
    except Exception as e:
        return(f"{RED}Erreur durant l'envoi de cert_request: {e}{RESET}")
    
def send_cert_reply(socket, cert_fn):
    try:
        message = "_Cert_Reply_"        # type de message
        intersection  = "_Intersection_"
        end = "_End_"
        with open(cert_fn, "rb") as cert_file:      # Charger lu certificat depuis le fichier
            cert_data = cert_file.read()

        socket.sendall(message.encode())        # 0
        socket.sendall(intersection.encode())   
        socket.sendall(myself.encode())         # 1 source
        socket.sendall(intersection.encode())  
        socket.sendall(cert_data)               # 2 certificat du demandeur
        socket.sendall(intersection.encode()) 
        socket.sendall(end.encode())

        return(f"{GREEN}certificat envoyé avec succes {RESET}")
    except Exception as e:
        return(f"{RED}Erreur durant l'envoi de cert_reply: {e}{RESET}")

def send_is_revoked_request(socket, cert_fn):   # retourne un booléen true revoqué false ok
    try:
        message = "_Is_Revoked_Request_"        # type de message
        intersection  = "_Intersection_"
        end = "_End_"
        with open(cert_fn, "rb") as cert_file:      # Charger lu certificat depuis le fichier
            cert_data = cert_file.read()

        socket.sendall(message.encode())        # 0
        socket.sendall(intersection.encode())   
        socket.sendall(myself.encode())         # 1 source 
        socket.sendall(intersection.encode())  
        socket.sendall(cert_data)               # 2 certificat a verifier
        socket.sendall(intersection.encode()) 
        socket.sendall(end.encode())

        print (f"{GREEN}demande de verification de validité envoyée à SCA avec succès{RESET}")
        return receive_message(socket)   # retourne un booléen true revoqué false ok
    except Exception as e:
        print (f"{RED}Erreur durant l'envoi de is_revoked_request ou de la reception de la reponse : {e}{RESET}")

def send_is_revoked_reply(socket, isrevoked, cert_data):  
    try:
        message = "_Is_Revoked_Reply_"          # type de message
        intersection  = "_Intersection_"
        end = "_End_"

        date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        signature = sign_message(myself + isrevoked + cert_data + date, myprivatekey)

        socket.sendall(message.encode())        # 0
        socket.sendall(intersection.encode())   
        socket.sendall(myself.encode())                 #  1 source
        socket.sendall(intersection.encode())  
        socket.sendall(isrevoked.encode())              # 2 true or false, true = revoqué false = non revoqué
        socket.sendall(intersection.encode()) 
        socket.sendall(cert_data.encode())              # 3 certificat 
        socket.sendall(intersection.encode())
        socket.sendall(date.encode())                   # 4 date
        socket.sendall(intersection.encode())
        socket.sendall(signature)                       # 5 signature
        socket.sendall(intersection.encode())
        socket.sendall(end.encode())

        return (f"{GREEN}réponse de verification de validité envoyée avec succès {RESET}")
    except Exception as e:
        print (f"{RED}Erreur durant l'envoi de is_revoked_reply: {e}{RESET}")

def send_symetric_key(socket, symetric_key_fn, public_key_fn):
    try:
        message = "_Send_Symetric_Key_"     # type de message
        intersection  = "_Intersection_"
        end = "_End_"
        with open(symetric_key_fn, 'rb') as key_file:       # Charger la clé depuis le fichier 
            key_data = key_file.read()

        with open(symetric_key_fn, "r") as key_file:
            str_key = key_file.read()

        socket.sendall(message.encode())        # 0
        socket.sendall(intersection.encode())   
        socket.sendall(myself.encode())         # source source
        socket.sendall(intersection.encode()) 
        socket.sendall(as_encrypt_message(str_key, public_key_fn).encode())               # 2 clé symétrique chiffrée
        socket.sendall(intersection.encode()) 
        socket.sendall(end.encode())

        return(f"{GREEN}clé symétrique envoyé avec succes{RESET}")
    except Exception as e:
        return(f"{RED}Erreur durant l'envoi de symetric_key: {e}{RESET}")

def send_message(socket, message2send):
    try:
        message = "_Message_"   # type de message
        intersection  = "_Intersection_"
        end = "_End_"
        date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        signature = sign_message(myself + message2send + date, myprivatekey)

        socket.sendall(message.encode())        # 0
        socket.sendall(intersection.encode())   
        socket.sendall(myself.encode())         #  1 source
        socket.sendall(intersection.encode())  
        socket.sendall(message2send.encode())   # 2 message a envoyer
        socket.sendall(intersection.encode()) 
        socket.sendall(date.encode())           # 3 date
        socket.sendall(intersection.encode())
        socket.sendall(signature)               # 5 signature
        socket.sendall(intersection.encode())
        socket.sendall(end.encode())

        return (f"{GREEN}Message envoyé avec succes {RESET}")
    except Exception as e:
        print (f"{RED}Erreur durant l'envoi de message : {e}{RESET}")


